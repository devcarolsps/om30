# Desafio OM30

- Foi Realizado um Teste para Vaga de PHP da empresa OM30.
- CRUD de paciente
 * Inserir um paciente com as devidas validações e inclusão de Imagem;
 * Atualizar um paciente com as devidas validações;
 * Deletar um paciente; 

## PASSO A PASSO

- git clone https://carolinesps@bitbucket.org/devcarolsps/om30.git;
- Abrir o Projeto
- Alterar o .env com os dados de acesso ao banco, (ou pode deixar o que está, que é meu de teste)
- Executar o comando para criar a base de dados
    * php spark migrate
- Executar o comando para levantar o servidor:
    * php spark serve
