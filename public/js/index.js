$(document).ready(function($)
{  
    
    // ----- MASCARAS  -----
    $('#cpf').mask('000.000.000-00', {reverse: true});
    $('#cep').mask('00000000', {reverse: true});



    // ----- BUSCAR CEP  -----
    $("#cep").focusout(function(){
        //Início do Comando AJAX
        $.ajax({
            //O campo URL diz o caminho de onde virá os dados
            //É importante concatenar o valor digitado no CEP
            url: 'https://viacep.com.br/ws/'+$(this).val()+'/json/unicode/',

            //Aqui você deve preencher o tipo de dados que será lido,
            //no caso, estamos lendo JSON.
            dataType: 'json',
            //SUCESS é referente a função que será executada caso
            //ele consiga ler a fonte de dados com sucesso.
            //O parâmetro dentro da função se refere ao nome da variável
            //que você vai dar para ler esse objeto.
            success: function(resposta){
                //Agora basta definir os valores que você deseja preencher
                //automaticamente nos campos acima.
                $("#logradouro").val(resposta.logradouro);
                $("#bairro").val(resposta.bairro);
                $("#cidade").val(resposta.localidade);
                $("#estado").val(resposta.uf);
                //Vamos incluir para que o Número seja focado automaticamente
                //melhorando a experiência do usuário
                $("#numero").focus();
            }
        });
    });


    
    // ----- VALIDA CPF  -----
    function validaCPF(cpf){
        var numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if(cpf.length < 11) return false;
        for(i = 0; i < cpf.length - 1; i++)
           if(cpf.charAt(i) != cpf.charAt(i + 1)){
              digitos_iguais = 0;
              break;
           }
        if(!digitos_iguais){
           numeros = cpf.substring(0,9);
           digitos = cpf.substring(9);
           soma = 0;
           for(i = 10; i > 1; i--)
              soma += numeros.charAt(10 - i) * i;
              resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
              if(resultado != digitos.charAt(0)) return false;
              numeros = cpf.substring(0,10);
              soma = 0;
              for(i = 11; i > 1; i--)
                 soma += numeros.charAt(11 - i) * i;
                 resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                 if(resultado != digitos.charAt(1)) return false;
           return true;
        }else return false;
    }

    
       


});