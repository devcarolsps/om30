<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class DbPacientes extends Migration
{
	public function up()
	{
		//
		$this->forge->addField([
			'id' => [
			   'type' => 'INT',
			   'constraint' => 5,
			   'unsigned' => true,
			   'auto_increment' => true,
			],
			'nome_pac' => [
			   'type' => 'VARCHAR',
			   'constraint' => '100',
			],
			'nome_mae' => [
			   'type' => 'VARCHAR',
			   'constraint' => '100',
			],
			'data_nasc' => [
			   'type' => 'DATE',
			],
			'cpf' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			],
			'cns' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			],
			'foto_perfil' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
                'null' => true,
			],
			'cep' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			],
			'logradouro' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			],
			'numero' => [
			   'type' => 'INT',
			   'constraint' => '10',
			],
			'complemento' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			   'null' => true,
			],
			'bairro' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			],
			'cidade' => [
			   'type' => 'VARCHAR',
			   'constraint' => '50',
			],
			'estado' => [
			   'type' => 'VARCHAR',
			   'constraint' => '10',
			],
			'created_date datetime default current_timestamp',
			'updated_date datetime default current_timestamp on update current_timestamp',
		  ]);
		  $this->forge->addKey('id', true);
		  $this->forge->createTable('db_pacientes');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('db_pacientes');
	}
}
