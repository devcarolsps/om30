<?php namespace App\Controllers;

use App\Models\Pacientes;

class PacientesController extends BaseController
{

   public function index(){
      $pacientes = new Pacientes();

      ## Fetch all records
      $data['pacientes'] = $pacientes->findAll();
      return view('pacientes/index',$data);
   }

   public function create(){
      return view('pacientes/create');
   }

   public function store(){
      $request = service('request');

      $postData = $request->getPost();

      //$cpf = trim($postData['cpf']);

      if(isset($postData['submit'])){

         ## Validation
         $input = $this->validate([
            'nome_pac' => 'required|min_length[3]',
            'cpf' => 'required|valid_cpf',
            'cns' => 'required|valid_cns',
            'foto_perfil' => [
               'uploaded[file]',
               'mime_in[file,image/jpg,image/jpeg,image/gif,image/png]',
               'max_size[file,4096]',
            ],

         ]);
        

         if (!$input) {
            return redirect()->route('pacientes/create')->withInput()->with('validation',$this->validator); 
         } else {

            $pacientes = new Pacientes();

            $avatar = $this->request->getFile('file');
            $avatarName = $avatar->getRandomName();
            $avatar->move(ROOTPATH . 'public/uploads',$avatarName);

            $data = [
               'nome_pac' => $postData['nome_pac'],
               'nome_mae' => $postData['nome_mae'],
               'data_nasc' => $postData['data_nasc'],
               'cpf' =>  $postData['cpf'],
               'cns' => $postData['cns'],
               'cep' => $postData['cep'],
               'logradouro' => $postData['logradouro'],
               'numero' => $postData['numero'],   
               'complemento' => $postData['complemento'],   
               'bairro' => $postData['bairro'],   
               'cidade' => $postData['cidade'],   
               'estado' => $postData['estado'],
               'foto_perfil' =>  $avatarName,

            ];


            ## Insert Record
            if($pacientes->insert($data)){
               session()->setFlashdata('message', 'Adicionado com Sucesso!');
               session()->setFlashdata('alert-class', 'alert-success');

               return redirect()->route('pacientes/create'); 
            }else{
               session()->setFlashdata('message', 'Erro ao Salvar!');
               session()->setFlashdata('alert-class', 'alert-danger');

               return redirect()->route('pacientes/create')->withInput(); 
            }

         }
      }

   }

   public function edit($id = 0){

      ## Select record by id
      $pacientes = new Pacientes();
      $paciente = $pacientes->find($id);

      $data['paciente'] = $paciente;
      return view('pacientes/edit',$data);

   }

   public function update($id = 0){
      $request = service('request');
      $postData = $request->getPost();

      if(isset($postData['submit'])){

        ## Validation
        $input = $this->validate([
            'nome_pac' => 'required|min_length[3]',
            'cpf' => 'required|valid_cpf',
            'cns' => 'required|valid_cns',
         ]);

        if (!$input) {
           return redirect()->route('pacientes/edit/'.$id)->withInput()->with('validation',$this->validator); 
        } else {

           $pacientes = new Pacientes();

            $data = [
               'nome_pac' => $postData['nome_pac'],
               'nome_mae' => $postData['nome_mae'],
               'data_nasc' => $postData['data_nasc'],
               'cpf' =>  $postData['cpf'],
               'cns' => $postData['cns'],
               'cep' => $postData['cep'],
               'logradouro' => $postData['logradouro'],
               'numero' => $postData['numero'],   
               'complemento' => $postData['complemento'],   
               'bairro' => $postData['bairro'],   
               'cidade' => $postData['cidade'],   
               'estado' => $postData['estado'],
            ];

           ## Update record
           if($pacientes->update($id,$data)){
              session()->setFlashdata('message', 'Atualização Realizada com Sucesso!');
              session()->setFlashdata('alert-class', 'alert-success');

              return redirect()->route('/'); 
           }else{
              session()->setFlashdata('message', 'Erro ao Atualizar!');
              session()->setFlashdata('alert-class', 'alert-danger');

              return redirect()->route('pacientes/edit/'.$id)->withInput(); 
           }

        }
      }

   }

   public function delete($id=0){

      $pacientes = new Pacientes();

      ## Check record
      if($pacientes->find($id)){

         ## Delete record
         $pacientes->delete($id);

         session()->setFlashdata('message', 'Deletado com Sucesso!');
         session()->setFlashdata('alert-class', 'alert-success');
      }else{
         session()->setFlashdata('message', 'Registro não encontrado!');
         session()->setFlashdata('alert-class', 'alert-danger');
      }

      return redirect()->route('/');

   }
}