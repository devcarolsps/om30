<?= $this->extend('layouts/layout') ?>

<?= $this->section('content') ?>

<div class="actionbutton mt-2">
  <a class="btn btn-info float-right mb20" href="<?=site_url('/')?>">Listar Pacientes</a>
</div>

<div class="row">
  <h2>Editar Paciente</h2>
</div>

<?php 
// Display Response
if(session()->has('message')){
?>
   <div class="alert <?= session()->getFlashdata('alert-class') ?>">
     <?= session()->getFlashdata('message') ?>
   </div>
<?php
}
?>

<?php $validation = \Config\Services::validation(); ?>
<div class="row">
  <div class="col-md-12">
    <form action="<?=site_url('pacientes/update/'.$paciente['id'])?>" method="post">
        <div class="form-grid">
            <div class="col">
                <img src="<?=base_url()?>/uploads/<?=$paciente['foto_perfil']?>" alt="Avatar" class="img-fluid img-thumbnail"/>
            </div> 


            <div class="row">
                <div class="col">
                    <label for="nome_pac">Nome Completo do Paciente: </label>
                    <input type="text" class="form-control" name="nome_pac" id="nome_pac" required value="<?= old('nome_pac',$paciente['nome_pac']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('nome_pac') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('nome_pac'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="nome_mae">Nome Completo da Mãe: </label>
                    <input type="text" class="form-control" name="nome_mae" id="nome_mae" required value="<?= old('nome_mae',$paciente['nome_mae']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('nome_mae') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('nome_mae'); ?>
                    </div>
                    <?php }?>
                </div>  
            </div>
            <div class="row">
                <div class="col">
                    <label for="data_nasc">Data de Nascimento: </label>
                    <input type="date" class="form-control" name="data_nasc" id="data_nasc" required value="<?= old('data_nasc', $paciente['data_nasc']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('data_nasc') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('data_nasc'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="cpf">CPF: </label>
                    <input type="text" class="form-control" name="cpf" id="cpf" required value="<?= old('cpf',$paciente['cpf']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('cpf') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('cpf'); ?>
                    </div>
                    <?php }?>
                </div>  
            </div>

            <div class="row">
                <div class="col">
                    <label for="cns">CNS: </label>
                    <input type="text" class="form-control" name="cns" id="cns" required value="<?= old('cns',$paciente['cns']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('cns') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('cns'); ?>
                    </div>
                    <?php }?>
                </div>  
            </div>

            <div class="row">
                <div class="col">
                    <label for="cep">CEP: </label>
                    <input type="text" class="form-control" name="cep" id="cep" required value="<?= old('cep',$paciente['cep']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('cep') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('cep'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="logradouro">Logradouro: </label>
                    <input type="text" class="form-control" name="logradouro" id="logradouro" required value="<?= old('logradouro',$paciente['logradouro']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('logradouro') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('logradouro'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="numero">Número: </label>
                    <input type="number" class="form-control" name="numero" required value="<?= old('numero',$paciente['numero']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('numero') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('numero'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="complemento">Complemento: </label>
                    <input type="text" class="form-control" name="complemento" required value="<?= old('complemento',$paciente['complemento']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('complemento') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('complemento'); ?>
                    </div>
                    <?php }?>
                </div>  
            </div>

            <div class="row">
                <div class="col">
                    <label for="bairro">Bairro: </label>
                    <input type="text" class="form-control" name="bairro" id="bairro" required value="<?= old('bairro',$paciente['bairro']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('bairro') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('bairro'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="cidade">Cidade: </label>
                    <input type="text" class="form-control" name="cidade" id="cidade" required value="<?= old('cidade', $paciente['cidade']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('cidade') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('cidade'); ?>
                    </div>
                    <?php }?>
                </div>  
                <div class="col">
                    <label for="estado">Estado: </label>
                    <input type="text" class="form-control" name="estado" id="estado"  required value="<?= old('estado', $paciente['estado']) ?>">
                    <!-- Error -->
                    <?php if( $validation->getError('estado') ) {?>
                    <div class='alert alert-danger mt-2'>
                        <?= $error = $validation->getError('estado'); ?>
                    </div>
                    <?php }?>
                </div>  
            </div>


        
      </div>
      <div class="form-group">
      </div>

      <button type="submit" class="btn btn-success" name="submit">Atualizar</button>
    </form>
  </div>

</div>
<script>
    function readURL(input, id) {
        id = id || '#preview_image';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(id)
                .attr('src', e.target.result)
                .width(200)
                .height(150);

            };
            reader.readAsDataURL(input.files[0]);

        }

    }

</script>

<?= $this->endSection() ?>