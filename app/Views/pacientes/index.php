<?= $this->extend('layouts/layout') ?>

<?= $this->section('content') ?>

<div class="actionbutton mt-2">
   <a class="btn btn-info float-right mb20" href="<?=site_url('pacientes/create')?>">Novo Paciente</a>
</div>

<?php 
// Display Response
if(session()->has('message')){
?>
   <div class="alert <?= session()->getFlashdata('alert-class') ?>">
      <?= session()->getFlashdata('message') ?>
   </div>
<?php
}
?>

<!-- Subject List -->
<table width="100%" border="1" style="border-collapse: collapse;" class="table table-striped">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Foto de Perfil</th>
      <th scope="col">Nome Completo do Paciente</th>
      <th scope="col">Nome Completo da Mãe</th>
      <th scope="col">Data de Nascimento</th>
      <th scope="col">CPF</th>
      <th scope="col">CNS</th>
      <th scope="col">Endereço Completo</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
  <?php 
  if(count($pacientes) > 0){

    foreach($pacientes as $paciente){
  ?>
     <tr>
       <td><?= $paciente['id'] ?></td>
       <td><img src="<?=base_url()?>/uploads/<?=$paciente['foto_perfil']?>" alt="Avatar" class="img-fluid img-thumbnail"/></td>
       <td><?= $paciente['nome_pac'] ?></td>
       <td><?= $paciente['nome_mae'] ?></td>
       <td><?= $paciente['data_nasc'] ?></td>
       <td><?= $paciente['cpf'] ?></td>
       <td><?= $paciente['cns'] ?></td>
       <td><?= $paciente['cep'] .'-'.$paciente['logradouro'].','.$paciente['numero']   ?></td>
       <td align="center">
         <a class="btn btn-sm btn-info" href="<?= site_url('pacientes/edit/'.$paciente['id']) ?>">Editar</a>
         <a class="btn btn-sm btn-danger" href="<?= site_url('pacientes/delete/'.$paciente['id']) ?>">Deletar</a>
       </td>
     </tr>
  <?php
    }

  }else{
  ?>
    <tr>
      <td colspan="12">Nenhum dado encontrado.</td>
    </tr>
  <?php
  }
  ?>
  </tbody>
</table>
<?= $this->endSection() ?>