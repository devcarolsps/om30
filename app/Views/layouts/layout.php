<!doctype html>
<html>
<head>
    <title>DESAFIO OM30</title>

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <style type="text/css">
    	.mb20{
    		margin-bottom: 20px;
    	}
    	.actionbutton{
		    width: 100%;
		    height: 55px;
		}
		.errors{
			color: red;
		}
    </style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?= $this->renderSection('content') ?>
			</div>
		</div>
		
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js" integrity="sha512-0XDfGxFliYJPFrideYOoxdgNIvrwGTLnmK20xZbCAvPfLGQMzHUsaqZK8ZoH+luXGRxTrS46+Aq400nCnAT0/w==" crossorigin="anonymous"></script>
	<script src="<?php echo base_url('js/index.js'); ?>" language="javascript" type="text/javascript"></script>
</body>
</html>
