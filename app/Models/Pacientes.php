<?php 
namespace App\Models;

use CodeIgniter\Model;

class Pacientes extends Model
{
    protected $table      = 'db_pacientes';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';

    protected $allowedFields = ['nome_pac','nome_mae','data_nasc','cpf','cns','foto_perfil','cep','logradouro','numero','complemento','bairro' ,'cidade' ,'estado' ];
    protected $useTimestamps = false;
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    
}